import { create } from "zustand";
import toast from "react-hot-toast";
import { calculateToastDuration } from "@/helpers/helpers.js";
import {
  getAllPokemonApi,
  getEvolutionChainApi,
  getPokemonLocationApi,
  getPokemonSpeciesApi,
  getPokemonTypesApi,
} from "@/api/pokemon/pokemonApi.js";

export const usePokemenStore = create((set, get) => ({
  pokemons: [],
  addPokemon: (data) => {
    set((state) => ({ pokemons: [...state.pokemons, data] }));
  },
  fixedPokemon: (data) => {
    set(() => ({ pokemons: data }));
  },
  isLoading: true,
  setIsLoading: (data) => {
    set((state) => ({ isLoading: data !== null ? data : !state.isLoading }));
  },
  getPokemonSpecies: async ({
    id,
    params = {},
    silent,
    onSuccess,
    onError,
  }) => {
    try {
      const response = await getPokemonSpeciesApi(id, params);
      const data = response;
      if (!silent) {
        const toastMessage = `Success to get pokemon species`;
        toast.success(toastMessage, {
          duration: calculateToastDuration(toastMessage),
          position: "top-right",
          className: "font-regular text-16",
        });
      }

      onSuccess?.(data);
      return data;
    } catch (error) {
      if (silent) return;
      const toastMessage = `Failed to get pokemon species`;
      toast.success(toastMessage, {
        duration: calculateToastDuration(toastMessage),
        position: "top-right",
        className: "font-regular text-16",
      });
    }
  },
  getPokemon: async ({ id, params = {}, silent, onSuccess, onError }) => {
    try {
      const response = await getAllPokemonApi(id, params);
      const data = response;
      if (!silent) {
        const toastMessage = `Success to get pokemon`;
        toast.success(toastMessage, {
          duration: calculateToastDuration(toastMessage),
          position: "top-right",
          className: "font-regular text-16",
        });
      }

      onSuccess?.(data);
      return data;
    } catch (error) {
      if (silent) return;
      const toastMessage = `Failed to get pokemon`;
      toast.success(toastMessage, {
        duration: calculateToastDuration(toastMessage),
        position: "top-right",
        className: "font-regular text-16",
      });
    }
  },
  getPokemonTypes: async ({ id, params = {}, silent, onSuccess, onError }) => {
    try {
      const response = await getPokemonTypesApi(id, params);
      const data = response;
      if (!silent) {
        const toastMessage = `Success to get pokemon types`;
        toast.success(toastMessage, {
          duration: calculateToastDuration(toastMessage),
          position: "top-right",
          className: "font-regular text-16",
        });
      }

      onSuccess?.(data);
      return data;
    } catch (error) {
      if (silent) return;
      const toastMessage = `Failed to get pokemon types`;
      toast.success(toastMessage, {
        duration: calculateToastDuration(toastMessage),
        position: "top-right",
        className: "font-regular text-16",
      });
    }
  },
  getPokemonLocation: async ({
    id,
    params = {},
    silent,
    onSuccess,
    onError,
  }) => {
    try {
      const response = await getPokemonLocationApi(id, params);
      const data = response;
      if (!silent) {
        const toastMessage = `Success to get pokemon location`;
        toast.success(toastMessage, {
          duration: calculateToastDuration(toastMessage),
          position: "top-right",
          className: "font-regular text-16",
        });
      }

      onSuccess?.(data);
      return data;
    } catch (error) {
      if (silent) return;
      const toastMessage = `Failed to get pokemon location`;
      toast.success(toastMessage, {
        duration: calculateToastDuration(toastMessage),
        position: "top-right",
        className: "font-regular text-16",
      });
    }
  },
  getEvolutionChain: async ({
    id,
    params = {},
    silent,
    onSuccess,
    onError,
  }) => {
    try {
      const response = await getEvolutionChainApi(id, params);
      const data = response;
      if (!silent) {
        const toastMessage = `Success to get pokemon evolution`;
        toast.success(toastMessage, {
          duration: calculateToastDuration(toastMessage),
          position: "top-right",
          className: "font-regular text-16",
        });
      }

      onSuccess?.(data);
      return data;
    } catch (error) {
      if (silent) return;
      const toastMessage = `Failed to get pokemon evolution`;
      toast.success(toastMessage, {
        duration: calculateToastDuration(toastMessage),
        position: "top-right",
        className: "font-regular text-16",
      });
    }
  },
}));

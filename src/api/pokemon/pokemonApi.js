import {
  POKEMON_EVOLUTION_CHAIN_URL,
  POKEMON_LOCATION_URL,
  POKEMON_SPECIES_URL,
  POKEMON_TYPE_URL,
  POKEMON_URL,
} from "@/api/urls.js";
import axiosInstance from "@/api/api.js";

export const getAllPokemonApi = (id = "", params) =>
  axiosInstance.get(`${POKEMON_URL}/${id}`, { params });

export const getEvolutionChainApi = (id = "", params) =>
  axiosInstance.get(`${POKEMON_EVOLUTION_CHAIN_URL}/${id}`, { params });

export const getPokemonLocationApi = (id = "", params) =>
  axiosInstance.get(`${POKEMON_LOCATION_URL}/${id}`, { params });

export const getPokemonSpeciesApi = (id = "", params) =>
  axiosInstance.get(`${POKEMON_SPECIES_URL}/${id}`, { params });

export const getPokemonTypesApi = (id = "", params) =>
  axiosInstance.get(`${POKEMON_TYPE_URL}/${id}`, { params });

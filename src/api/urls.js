export const BASE_URL = import.meta.env.VITE_BACKEND_BASE_URL;
export const POKEMON_EVOLUTION_CHAIN_URL = BASE_URL + "/evolution-chain";
export const POKEMON_URL = BASE_URL + "/pokemon";
export const POKEMON_LOCATION_URL = BASE_URL + "/location";
export const POKEMON_SPECIES_URL = BASE_URL + "/pokemon-species";
export const POKEMON_TYPE_URL = BASE_URL + "/type";

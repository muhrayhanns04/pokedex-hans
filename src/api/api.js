import axios from "axios";
import Cookies from "js-cookie";
import { BASE_URL } from "@/api/urls.js";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

// const HEADERS = "Authorization";

axiosInstance.interceptors.request.use((req) => {
  // const tokens = JSON.parse(Cookies.get("server_provide"));
  // if (tokens) {
  //   req.headers[HEADERS] = "Bearer " + tokens.provide;
  // }
  return req;
});

export default axiosInstance;

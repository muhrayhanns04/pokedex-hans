export const colorMapping = {
  normal: "#CCCCCC",
  fighting: "#D56723",
  flying: "#3DC7EF",
  poison: "#B97FC9",
  ground: "#F7DE3F",
  rock: "#A38C21",
  bug: "#729F3F",
  ghost: "#7B62A3",
  steel: "#9EB7B8",
  fire: "#FD7D24",
  water: "#4592C4",
  grass: "#9BCC50",
  electric: "#EED535",
  psychic: "#F366B9",
  ice: "#51C4E7",
  dragon: "#F16E57",
  dark: "#080808",
  fairy: "#FDB9E9",
  unknown: "#464c55",
  shadow: "#3B3B3B",
};

export const colorVariants = {
  normal: {
    container: "hover:bg-[#CCCCCC] bg-gray-30",
    text: "group-hover:text-white text-[#CCCCCC]",
  },
  fighting: {
    container: "hover:bg-[#D56723] bg-gray-30",
    text: "group-hover:text-white text-[#D56723]",
  },
  flying: {
    container: "hover:bg-[#3DC7EF] bg-gray-30",
    text: "group-hover:text-white text-[#3DC7EF]",
  },
  poison: {
    container: "hover:bg-[#B97FC9] bg-gray-30",
    text: "group-hover:text-white text-[#B97FC9]",
  },
  ground: {
    container: "hover:bg-[#F7DE3F] bg-gray-30",
    text: "group-hover:text-white text-[#F7DE3F]",
  },
  rock: {
    container: "hover:bg-[#A38C21] bg-gray-30",
    text: "group-hover:text-white text-[#A38C21]",
  },
  bug: {
    container: "hover:bg-[#729F3F] bg-gray-30",
    text: "group-hover:text-white text-[#729F3F]",
  },
  ghost: {
    container: "hover:bg-[#7B62A3] bg-gray-30",
    text: "group-hover:text-white text-[#7B62A3]",
  },
  steel: {
    container: "hover:bg-[#9EB7B8] bg-gray-30",
    text: "group-hover:text-white text-[#9EB7B8]",
  },
  fire: {
    container: "hover:bg-[#FD7D24] bg-gray-30",
    text: "group-hover:text-white text-[#FD7D24]",
  },
  water: {
    container: "hover:bg-[#4592C4] bg-gray-30",
    text: "group-hover:text-white text-[#4592C4]",
  },
  grass: {
    container: "hover:bg-[#9BCC50] bg-gray-30",
    text: "group-hover:text-white text-[#9BCC50]",
  },
  electric: {
    container: "hover:bg-[#EED535] bg-gray-30",
    text: "group-hover:text-white text-[#EED535]",
  },
  psychic: {
    container: "hover:bg-[#F366B9] bg-gray-30",
    text: "group-hover:text-white text-[#F366B9]",
  },
  ice: {
    container: "hover:bg-[#51C4E7] bg-gray-30",
    text: "group-hover:text-white text-[#51C4E7]",
  },
  dragon: {
    container: "hover:bg-[#F16E57] bg-gray-30",
    text: "group-hover:text-white text-[#F16E57]",
  },
  dark: {
    container: "hover:bg-[#080808] bg-gray-30",
    text: "group-hover:text-white text-[#080808]",
  },
  fairy: {
    container: "hover:bg-[#FDB9E9] bg-gray-30",
    text: "group-hover:text-white text-[#FDB9E9]",
  },
  unknown: {
    container: "hover:bg-[#464c55] bg-gray-30",
    text: "group-hover:text-white text-[#464c55]",
  },
  shadow: {
    container: "hover:bg-[#3B3B3B] bg-gray-30",
    text: "group-hover:text-white text-[#3B3B3B]",
  },
};

export function mapColorsToVariants(mapping) {
  const colorVariants = {};

  for (const key in mapping) {
    colorVariants[key] = {
      container: `hover:bg-[${mapping[key]}] bg-gray-30`,
      text: `group-hover:text-white text-[${mapping[key]}]`,
      coloredText: `text-[${mapping[key]}]`,
    };
  }

  return colorVariants;
}

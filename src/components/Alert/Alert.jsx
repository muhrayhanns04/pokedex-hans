import {
	BsFillCheckCircleFill,
	BsFillExclamationCircleFill,
	BsFillInfoCircleFill,
	BsFillXCircleFill,
} from "react-icons/bs";

import PropTypes from "prop-types";
import React from "react";

const Alert = (props) => {
	const { options, message } = props;

	const optionsType = {
		info: {
			icon: <BsFillInfoCircleFill className="text-gray-400" />,
			style: "bg-white text-gray-500",
		},
		success: {
			icon: <BsFillCheckCircleFill />,
			style: "bg-green-500 text-white",
		},
		warning: {
			icon: <BsFillExclamationCircleFill />,
			style: "bg-yellow-500 text-white",
		},
		error: {
			icon: <BsFillXCircleFill />,
			style: "bg-red-500 text-white",
		},
	};

	return (
		<div
			className={`mt-16 flex flex-row items-center shadow-md rounded-16 py-13 px-16 ${
				optionsType[options.type]?.style
			}`}
		>
			{optionsType[options.type]?.icon}
			<p className="ml-8 font-regular">{message}</p>
		</div>
	);
};

Alert.propTypes = {
	message: PropTypes.string.isRequired,
	options: PropTypes.shape({
		type: PropTypes.oneOf(["info", "success", "warning", "error"]),
	}),
};

export default Alert;

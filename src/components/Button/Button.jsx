import PropTypes from "prop-types";
import React from "react";
import { Spinner } from "@chakra-ui/react";

const Button = (props) => {
	const {
		value,
		className,
		textClass,
		isLoading,
		size,
		icon,
		disabled,
		type,
		...attrs
	} = props;

	const fontSize = {
		sm: "text-14",
		md: "text-18",
		lg: "text-24",
	};

	const buttonPadding = {
		sm: "px-[24px] py-[10px]",
		md: "px-32 py-13",
		lg: "px-24 py-13",
	};

	return (
		<button
			{...attrs}
			type={type}
			target="_blank"
			disabled={disabled}
			className={` no-decoration flex cursor-pointer flex-row items-center justify-center space-x-16 rounded-8 no-underline ${
				disabled &&
				"cursor-not-allowed bg-gray-300 text-gray-500 hover:bg-gray-300 hover:text-gray-500"
			} button-transition ${className} ${buttonPadding[size]}`}
		>
			{icon}
			{isLoading ? (
				<Spinner />
			) : (
				<span
					className={`no-decoration text-center font-medium ${fontSize[size]} no-underline ${textClass}`}
				>
					{value}
				</span>
			)}
		</button>
	);
};

Button.propTypes = {
	value: PropTypes.string,
	className: PropTypes.string,
	textClass: PropTypes.string,
	isLoading: PropTypes.bool,
	size: PropTypes.oneOf(["sm", "md", "lg"]),
	type: PropTypes.oneOf(["submit", "button"]),
	onClick: PropTypes.func,
	icon: PropTypes.element,
	disabled: PropTypes.bool,
};

Button.defaultProps = {
	type: "submit",
	className: "button-transition",
	size: "sm",
};

export default Button;

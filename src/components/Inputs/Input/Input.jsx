import { TbEye, TbEyeOff } from "react-icons/tb";

import React, { useState } from "react";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";

const Input = ({
  label,
  className,
  secondaryLabel,
  secondaryLabelClassName,
  inputClassName,
  name,
  control,
  register,
  required,
  marginAuto,
  errors,
  optional,
  disabled,
  allFieldsAreDisabled,
  afterOnChange,
  type,
  password,
  leftIcon,
  onSubmit,
  onKeyDown,
  rightIcon,
  ...attrs
}) => {
  const [isVisible, setIsVisible] = useState(true);

  const handleVisible = () => setIsVisible(!isVisible);

  return (
    <div className={`relative ${className}`}>
      {label && (
        <p className="mb-8 flex flex-row items-center text-14 font-medium text-gray-500">
          {label}
          {/* required sign */}
          {required && <span className="ml-8 text-red-500"> *</span>}
          {/* optional sign */}
          {optional && (
            <span className="ml-8 font-regular text-gray-300">(optional)</span>
          )}
          {/* secondary label */}
          <span
            className={`${secondaryLabelClassName || "ml-8 text-blue-500"}`}
          >
            {secondaryLabel}
          </span>
        </p>
      )}

      <div className="relative flex w-full flex-col items-start">
        <div className="relative flex w-full flex-row items-center">
          {leftIcon && <div className="absolute left-8 z-10">{leftIcon}</div>}
          {rightIcon && (
            <div className="absolute right-16 z-10">{rightIcon}</div>
          )}
          {control ? (
            <Controller
              name={name}
              control={control}
              render={({ field: { onChange, ref, value } }) => {
                return (
                  <input
                    onChange={(event) => {
                      onChange(event.target.value);
                      afterOnChange &&
                        afterOnChange(event.target.value, event, onChange);
                    }}
                    ref={ref}
                    value={value}
                    onKeyDown={onKeyDown}
                    type={
                      password && isVisible ? "password" : type ? type : "text"
                    }
                    autoComplete="on"
                    disabled={allFieldsAreDisabled || disabled}
                    className={`input-border relative w-full rounded-8 font-regular text-16 text-gray-500 placeholder-gray-200 ${
                      disabled
                        ? "cursor-not-allowed bg-gray-20 text-gray-300"
                        : ""
                    } ${inputClassName} ${errors && "border-red-400"}`}
                    style={{
                      WebkitBoxShadow: "0 0 0px 0px #FFFFFF",
                    }}
                    {...attrs}
                  />
                );
              }}
            />
          ) : null}

          {register ? (
            <input
              onKeyDown={onKeyDown}
              {...register(name)}
              type={password && isVisible ? "password" : type ? type : "text"}
              autoComplete="on"
              disabled={allFieldsAreDisabled || disabled}
              className={`input-border relative w-full rounded-8 font-regular text-16 text-gray-500 placeholder-gray-200 ${
                disabled ? "cursor-not-allowed bg-gray-20 text-gray-300" : ""
              } ${inputClassName} ${errors && "border-red-400"}`}
              style={{
                WebkitBoxShadow: "0 0 0px 0px #FFFFFF",
              }}
              {...attrs}
            />
          ) : null}

          {password && (
            <button
              type="button"
              onClick={handleVisible}
              className="absolute right-16 z-10"
            >
              {isVisible && <TbEye className="text-24 text-gray-400" />}
              {!isVisible && <TbEyeOff className="text-24 text-gray-400" />}
            </button>
          )}
        </div>
        {/* error message */}
        {errors && <p className="error-message mt-4">{errors?.message}</p>}
      </div>
    </div>
  );
};

Input.propTypes = {
  control: PropTypes.any,
  register: PropTypes.any,
  afterOnChange: PropTypes.func,
  type: PropTypes.string,
  password: PropTypes.bool,
  leftIcon: PropTypes.any,
  rightIcon: PropTypes.any,
  onSubmit: PropTypes.func,
  onKeyDown: PropTypes.func,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  secondaryLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  secondaryLabelClassName: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  inputClassName: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  errors: PropTypes.object,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  optional: PropTypes.bool,
  marginAuto: PropTypes.bool,
  allFieldsAreDisabled: PropTypes.bool,
  autoComplete: PropTypes.oneOf(["on", "off"]),
};

Input.defaultProps = {
  autoComplete: "off",
  required: false,
  disabled: false,
  marginAuto: true,
  type: "text",
};

export default Input;

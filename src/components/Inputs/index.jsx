import ColorPicker from "./ColorPicker/ColorPicker";
import CreatableSelects from "./CreatableSelect/CreateableSelect";
import FilterSelect from "./FilterSelect/FilterSelect";
import Input from "./Input/Input";
import Selects from "./Select/Select";
import TextArea from "./TextArea/TextArea";

Input.Select = Selects;
Input.CreatableSelect = CreatableSelects;
Input.TextArea = TextArea;
Input.ColorPicker = ColorPicker;
Input.FilterSelect = FilterSelect;

export default Input;

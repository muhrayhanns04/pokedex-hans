import Select, { components } from "react-select";

import { AsyncPaginate } from "react-select-async-paginate";
import { Controller } from "react-hook-form";
import React from "react";

const Placeholder = (props) => {
  return <components.Placeholder {...props} />;
};
const FilterSelect = ({
  data = [],
  name,
  required = false,
  onBlur,
  control,
  background,
  className,
  placeholder,
  icon,
  afterOnChange,
  bordered,
  onGetData,
  totalRecords,
}) => {
  const [valueSelected, setValueSelected] = React.useState();

  const DropdownIndicator = (props) => {
    return (
      <components.DropdownIndicator {...props}>
        {icon}
      </components.DropdownIndicator>
    );
  };

  const styles = {
    menu: (provided, state) => ({
      ...provided,
      zIndex: 99999,
    }),
    option: (provided, state) => ({
      ...provided,
      fontFamily: "Regular",
      fontSize: 14,
      color: state.isSelected ? "#FFFFFF" : "#495057",
    }),
    singleValue: (provided, state) => ({
      ...provided,
      fontFamily: "Regular",
      fontSize: 14,
      color: "#495057",
      backgroundColor: background && "#FFFFFF",
    }),
    control: (provided, state) => ({
      ...provided,
      backgroundColor: background && "#FFFFFF",
      borderWidth: !background && 2,
      borderColor: !background && "#E0E0E0",
      border: bordered && "1px solid #e1e4e8",
      fontFamily: "Regular",
      fontSize: 14,
      color: "#495057",
      width: "100%",
      boxShadow: "none",
      "&:hover": {
        borderColor: "#ffc501",
      },
      cursor: "pointer",
      borderRadius: 8,
    }),
    input: (provided, state) => ({
      ...provided,
    }),
    menuPortal: (provided) => ({ ...provided, zIndex: 9999 }),
  };

  const loadOptions = async (searchQuery, loadedOptions, { page }) => {
    onGetData(page);

    return {
      options: data,
      hasMore: data?.length < totalRecords,
      additional: {
        page: page + 1,
      },
    };
  };

  return (
    <div className={`${className} min-w-[160px] cursor-pointer`}>
      <Controller
        control={control}
        name={name}
        rules={{ required }}
        render={({ field: { onChange, ref } }) => (
          <AsyncPaginate
            components={{
              Placeholder,
              DropdownIndicator,
              IndicatorSeparator: () => null,
            }}
            placeholder={placeholder}
            menuPortalTarget={document.body}
            menuPosition="fixed"
            loadOptions={loadOptions}
            styles={styles}
            value={valueSelected}
            onBlur={onBlur}
            inputRef={ref}
            isClearable
            onChange={(val) => {
              onChange(val?.value);
              setValueSelected(val);
              afterOnChange && afterOnChange(val);
            }}
            additional={{
              page: 1,
            }}
          />
        )}
      />
    </div>
  );
};

export default FilterSelect;

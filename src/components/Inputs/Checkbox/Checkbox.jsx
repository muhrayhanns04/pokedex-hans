import InputHook from "../InputHook";
import React from "react";

const Checkbox = ({
	register,
	label,
	content,
	name,
	value,
	checked,
	required,
}) => {
	return (
		<div className="mb-16 flex flex-row items-center">
			<InputHook
				register={register}
				value={value}
				name={name}
				type="radio"
				required={required}
				checked={checked}
				className="mb-0 mt-0"
				inputClassName="h-24 w-24 rounded-full"
			/>
			{content ? (
				content
			) : (
				<p className={`ml-16 text-16 text-gray-400`}>{label}</p>
			)}
		</div>
	);
};

export default Checkbox;

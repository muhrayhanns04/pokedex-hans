import { Controller } from "react-hook-form";
import PropTypes from "prop-types";
import React from "react";
import Select from "react-select";
import { TbX } from "react-icons/tb";

const IndicatorSeparator = ({ innerProps }) => {
  return <span {...innerProps} />;
};

IndicatorSeparator.propTypes = {
  innerProps: PropTypes.any,
};

const CustomClearText = () => (
  <TbX className="text-16 text-gray-200 hover:text-red-500" />
);

const ClearIndicator = (props) => {
  const {
    children = <CustomClearText />,
    getStyles,
    innerProps: { ref, ...restInnerProps },
  } = props;

  return (
    <div
      {...restInnerProps}
      ref={ref}
      style={{ ...getStyles("clearIndicator", props), marginRight: -16 }}
    >
      <div style={{ padding: "0px 5px" }}>{children}</div>
    </div>
  );
};

ClearIndicator.propTypes = {
  children: PropTypes.any,
  getStyles: PropTypes.func,
  innerProps: PropTypes.any,
};

const Selects = ({
  label,
  data,
  name,
  required,
  onBlur,
  control,
  background,
  errors,
  className,
  placeholder = "Please choose",
  afterOnChange,
  disabled,
  onInputChange,
  menuStyles,
  isMulti,
}) => {
  const styles = {
    menuList: (base) => ({
      ...base,
      margin: 8,
      "::-webkit-scrollbar": {
        width: "0",
        height: "0px",
      },
      "::-webkit-scrollbar-track": {
        background: "#f1f1f1",
        borderRadius: "0 8px 0 0 ",
      },
      "::-webkit-scrollbar-thumb": {
        background: "#888",
        borderRadius: 99,
      },
      "::-webkit-scrollbar-thumb:hover": {
        background: "#555",
        borderRadius: 99,
      },
    }),
    option: (provided, state) => {
      return {
        ...provided,
        fontFamily: state.isSelected ? "Semibold" : "Regular",
        fontSize: 13,
        padding: "8px 16px",
        color: state.isSelected ? "#FFFFFF" : "#1d1b1d",
        borderRadius: 8,
      };
    },
    singleValue: (provided) => ({
      ...provided,
      fontFamily: "Regular",
      fontSize: 13,
      color: "#080808",
    }),
    control: (provided, state) => ({
      ...provided,
      backgroundColor:
        background && !disabled ? "#f2f2f2" : disabled ? "#FAFAFA" : "#FFFFFF",
      border: background && "none",
      fontFamily: "Regular",
      fontSize: 13,
      width: "100%",
      borderRadius: 8,
    }),
    input: (provided) => ({
      ...provided,
      padding: "8px",
      ...menuStyles,
    }),
    menuPortal: (provided) => ({
      ...provided,
      zIndex: 999,
    }),
    menu: (provided) => ({
      ...provided,
      zIndex: 999,
      borderRadius: "8px 8px 8px 8px",
    }),
  };

  return (
    <div className={`min-w-[160px] ${className}`}>
      {label && (
        <p className="mb-8 text-14 font-medium text-gray-900">
          {label}
          {required && <span className="text-red-500"> *</span>}
        </p>
      )}

      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, ref, value } }) => {
          return (
            <Select
              isMulti={isMulti}
              components={{ IndicatorSeparator, ClearIndicator }}
              isDisabled={disabled}
              menuPortalTarget={document.body}
              menuPosition="fixed"
              options={data}
              styles={styles}
              value={value}
              onBlur={onBlur}
              inputRef={ref}
              onChange={(val) => {
                onChange(val);
                afterOnChange && afterOnChange(val);
              }}
              onInputChange={onInputChange}
              placeholder={placeholder}
              isClearable
              theme={(theme) => ({
                ...theme,
                borderRadius: 0,
                colors: {
                  ...theme.colors,
                  primary25: "#f9f9fb",
                  primary50: "#f9f9fb",
                  primary: "#D7A137",
                  neutral20: "#e1e4e8",
                },
              })}
            />
          );
        }}
      />
      {errors && (
        <p className="error-message mt-4">
          {errors?.message || errors?.label?.message}
        </p>
      )}
    </div>
  );
};

Selects.propTypes = {
  afterOnChange: PropTypes.func,
  background: PropTypes.string,
  className: PropTypes.string,
  control: PropTypes.any,
  isMulti: PropTypes.any,
  data: PropTypes.array,
  disabled: PropTypes.any,
  errors: PropTypes.shape({
    label: PropTypes.shape({
      message: PropTypes.any,
    }),
    message: PropTypes.any,
  }),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  menuStyles: PropTypes.any,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onBlur: PropTypes.any,
  onInputChange: PropTypes.any,
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  required: PropTypes.bool,
};

Selects.defaultProps = {
  required: false,
};

export default Selects;

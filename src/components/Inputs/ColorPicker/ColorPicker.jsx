import Item from "./partials/Item";
import React from "react";
import colors from "./api/colors.json";

const ColorPicker = ({
  label,
  className,
  secondaryLabel,
  secondaryLabelClassName,
  required,
  marginAuto,
  optional,
  afterOnChange,
}) => {
  const [isSelected, setIsSelected] = React.useState();

  const handleSelected = (item) => {
    setIsSelected(item);
  };

  return (
    <div className={`relative ${marginAuto && "mb-13"} ${className}`}>
      {label && (
        <p className="mb-8 text-14 font-medium text-gray-500">
          {label}

          {/* required sign */}
          {required && <span className="text-red-500">* </span>}

          {/* optional sign */}
          {optional && (
            <span className="font-regular text-gray-300">(optional)</span>
          )}

          {/* secondary label */}
          <span className={`${secondaryLabelClassName || "text-blue-500"}`}>
            {secondaryLabel}
          </span>
        </p>
      )}

      <div className="grid grid-cols-9 w-fit gap-13 mt-8">
        {colors.map((item, index) => (
          <Item
            afterOnChange={afterOnChange}
            isSelected={isSelected}
            handleSelected={handleSelected}
            key={index}
            item={item}
          />
        ))}
      </div>
    </div>
  );
};

export default ColorPicker;

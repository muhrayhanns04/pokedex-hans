import React from "react";

const Item = ({ afterOnChange, handleSelected, item, isSelected }) => {
	return (
		<div
			onClick={() => {
				afterOnChange(item);
				handleSelected(item);
			}}
			className={`w-32 h-32 rounded-full cursor-pointer ${
				isSelected?.color === item.color && " border-[5px]"
			}`}
			style={{
				backgroundColor:
					isSelected?.color === item.color ? "#FFF" : item.color,
				borderColor: isSelected?.color === item.color && item.color,
			}}
		/>
	);
};

export default Item;

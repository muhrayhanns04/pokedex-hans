import PropTypes from "prop-types";
import React from "react";

const TextArea = ({
	label,
	secondaryLabel,
	required,
	optional,
	name,
	type,
	register,
	className,
	autocomplete = "off",
	placeholder,
	inputClassName,
	disabled,
	errors,
	rows = 5,
	...attrs
}) => {
	return (
		<div className={`relative ${className}`}>
			{label && (
				<p className="mb-8 font-medium text-14 text-gray-500">
					{label}
					{required && <span className="text-red-500"> *</span>}
					{optional && (
						<span className="font-regular text-gray-300">
							(optional)
						</span>
					)}
				</p>
			)}

			<div className="relative flex w-full flex-col">
				<textarea
					{...register(name)}
					{...attrs}
					rows={rows}
					type={type ? type : type === "search" ? "text" : "text"}
					autoComplete={autocomplete}
					className={`input-border relative w-full rounded-8  placeholder-gray-200 ${inputClassName} ${
						errors && "border-red-400"
					}  font-regular text-14 text-gray-900 focus:border-none focus:outline focus:outline-2 focus:outline-offset-0 focus:outline-yellow-600`}
				></textarea>

				{errors && (
					<p className="mt-[4px] font-regular text-14 text-red-500">
						{errors?.message}
					</p>
				)}
			</div>
		</div>
	);
};

TextArea.propTypes = {
	label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	secondaryLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	className: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	inputClassName: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	required: PropTypes.bool,
	disabled: PropTypes.bool,
	type: PropTypes.oneOf(["search", "text"]),
	autocomplete: PropTypes.oneOf(["on", "off"]),
};

export default TextArea;

import React from "react";

const Badge = ({ value, className }) => {
	return (
		<span
			className={`py-4 bg-gray-20 px-10 rounded-full w-fit h-fit font-semi-bold text-12 text-gray-500 ${className}`}
		>
			{value}
		</span>
	);
};

export default Badge;

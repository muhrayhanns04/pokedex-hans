import React from "react";
import { Cards, Sidebar, Header } from "./partials";

const HomePage = () => {
  return (
    <div className="w-full max-h-screen min-h-screen grid grid-cols-[max-content,1fr] gap-16 border border-red-600 overflow-hidden">
      <Sidebar />
      <div className="w-full relative grid grid-cols-1 grid-rows-[max-content,1fr] bg-gray-30 space-y-12 place-content-start py-16 overflow-hidden">
        <Header />
        <Cards />
      </div>
    </div>
  );
};

export default HomePage;

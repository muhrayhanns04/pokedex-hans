import React, { useEffect, useState } from "react";
import { usePokemenStore } from "@/api/pokemon/usePokemonStore.js";
import {
  colorMapping,
  colorVariants,
  mapColorsToVariants,
} from "@/api/colors.js";

const Sidebar = () => {
  const [types, setTypes] = useState([]);
  const [
    getPokemonTypes,
    fixedPokemon,
    getPokemon,
    getPokemonSpecies,
    setIsLoading,
  ] = usePokemenStore((state) => [
    state.getPokemonTypes,
    state.fixedPokemon,
    state.getPokemon,
    state.getPokemonSpecies,
    state.setIsLoading,
  ]);

  useEffect(() => {
    getPokemonTypes({
      onSuccess: async (typeNonDetail) => {
        const results = typeNonDetail?.data?.results;
        setTypes(results);

        const pokemonPromises = results?.map(async (item) => {
          const types = await getPokemonTypes({ id: item?.name });
          return types?.data;
        });

        const allPokemonData = await Promise.all(pokemonPromises);
        setTypes(allPokemonData);
      },
    });
  }, []);

  const getPokemenByType = (type) => {};

  return (
    <div className="bg-white flex flex-col space-y-8 max-h-screen overflow-y-scroll">
      {types?.map((type, key) => (
        <div
          className={`group cursor-pointer flex items-center justify-center px-16 py-8 rounded-8 ${
            colorVariants[type?.name]?.container
          }`}
          key={key}
          onClick={async () => {
            setIsLoading(true);
            const pokemonPromises = type?.pokemon?.map(async (item) => {
              const pokemon = await getPokemon({ id: item?.pokemon?.name });
              let species = null;
              if (pokemon?.data?.name === pokemon?.data?.species) {
                species = await getPokemonSpecies({
                  id: pokemon?.data?.id,
                });
              }

              let data = {
                id: pokemon?.data?.id,
                name: pokemon?.data?.name,
                abilities: pokemon?.data?.abilities,
                height: pokemon?.data?.height,
                weight: pokemon?.data?.weight,
                moves: pokemon?.data?.moves,
                sprites: pokemon?.data?.sprites,
                stats: pokemon?.data?.stats,
                types: pokemon?.data?.types,
              };

              if (species) {
                data = {
                  ...data,
                  speciesDetail: {
                    base_happiness: species?.data?.base_happiness,
                    genera: species?.data?.genera,
                    habitat: species?.data?.habitat,
                    shape: species?.data?.shape,
                  },
                };
              }

              return data;
            });
            const allPokemonData = await Promise.all(pokemonPromises);
            fixedPokemon(allPokemonData);

            setTimeout(() => {
              setIsLoading(false);
            }, 500);
          }}
        >
          <p className={`font-bold text-14 ${colorVariants[type?.name]?.text}`}>
            {type?.name.toUpperCase()}
          </p>
        </div>
      ))}
    </div>
  );
};

export default Sidebar;

import React from "react";

const Header = () => {
  return (
    <div className="bg-blue-600 py-16 flex flex-col">
      <h1 className="font-bold text-24 text-yellow-400 text-center">
        Complete Pokémon Pokédex
      </h1>
    </div>
  );
};

export default Header;

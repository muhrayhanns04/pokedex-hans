import React from "react";

const PokeContainer = ({ index, children }) => {
  return (
    <div
      className={`grid grid-cols-[max-content,0.6fr,1fr] py-16 gap-16 px-16 ${
        index % 2 === 1 ? "bg-gray-30" : "bg-white"
      }`}
    >
      {children}
    </div>
  );
};

export default PokeContainer;

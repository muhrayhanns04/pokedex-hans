import React, { useEffect } from "react";
import Button from "@/components/Button/index.jsx";
import { usePokemenStore } from "@/api/pokemon/usePokemonStore.js";

const PokeButtonDetail = ({ pokemon, setIsOpen, setPokemonEvolution }) => {
  const [getEvolutionChain, getPokemon, getPokemonSpecies] = usePokemenStore(
    (state) => [
      state.getEvolutionChain,
      state.getPokemon,
      state.getPokemonSpecies,
    ]
  );

  function formatEvolutions(inputData) {
    const evolutionChain = inputData.chain;

    const evolutions = [];

    function collectEvolutions(chain) {
      evolutions.push({
        name: chain.species.name,
        url: chain.species.url,
      });

      if (chain.evolves_to.length > 0) {
        collectEvolutions(chain.evolves_to[0]);
      }
    }

    collectEvolutions(evolutionChain);

    const formattedData = {
      id: inputData.id,
      evolutions: evolutions,
    };

    return formattedData;
  }

  useEffect(() => {
    getEvolutionChain({
      id: pokemon?.id,
      onSuccess: async (data) => {
        const pokemonPromises = formatEvolutions(data?.data)?.evolutions?.map(
          async (item) => {
            const pokemon = await getPokemon({ id: item?.name });
            let species = null;
            if (pokemon?.data?.name === pokemon?.data?.species) {
              species = await getPokemonSpecies({
                id: pokemon?.data?.id,
              });
            }

            let data = {
              id: pokemon?.data?.id,
              name: pokemon?.data?.name,
              abilities: pokemon?.data?.abilities,
              height: pokemon?.data?.height,
              weight: pokemon?.data?.weight,
              moves: pokemon?.data?.moves,
              sprites: pokemon?.data?.sprites,
              stats: pokemon?.data?.stats,
              types: pokemon?.data?.types,
            };

            if (species) {
              data = {
                ...data,
                speciesDetail: {
                  base_happiness: species?.data?.base_happiness,
                  genera: species?.data?.genera,
                  habitat: species?.data?.habitat,
                  shape: species?.data?.shape,
                },
              };
            }

            return data;
          }
        );
        const allPokemonData = await Promise.all(pokemonPromises);
        setPokemonEvolution(allPokemonData);
      },
    });
  }, [pokemon?.id]);

  return (
    <div className="flex flex-col justify-center w-fit">
      <Button
        value="Show Details"
        onClick={() => {
          setIsOpen((prev) => !prev);
        }}
      />
    </div>
  );
};

export default PokeButtonDetail;

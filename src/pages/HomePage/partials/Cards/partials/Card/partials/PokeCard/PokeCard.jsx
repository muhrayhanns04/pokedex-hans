import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { formatNumericValue } from "@/helpers/helpers.js";
import { capitalize } from "lodash";
import { colorVariants } from "@/api/colors.js";

const PokeCard = ({ formattedIndex, pokemon }) => {
  return (
    <div className="bg-white shadow-lg relative flex flex-col items-center justify-center space-y-8 p-24 w-[400px] h-auto rounded-24">
      <div className="relative w-full flex flex-col items-center justify-center">
        <p className="absolute font-bold text-[120px] text-gray-100 ">
          #{formattedIndex}
        </p>
        <LazyLoadImage
          src={pokemon?.sprites?.front_default}
          className="w-[92px] z-20"
        />
        <div className="bg-white shadow-lg absolute -top-42 -left-42 p-16 aspect-square z-20 flex flex-col items-center justify-center rounded-full">
          <p className="font-semibold text-16 text-gray-500">
            {formatNumericValue(pokemon?.height / 10, {
              suffix: "m",
              decimals: 1,
            })}
          </p>
        </div>
        <div className="bg-white shadow-lg absolute -top-42 -right-42 p-16 aspect-square z-20 flex flex-col items-center justify-center rounded-full">
          <p className="font-semibold text-16 text-gray-500">
            {formatNumericValue(pokemon?.weight / 10, {
              suffix: "Kg",
              decimals: 1,
            })}
          </p>
        </div>
      </div>

      <div className="flex flex-col items-center justify-center space-y-12">
        <div className="flex flex-col items-center justify-center">
          <h1 className="font-bold text-24 text-gray-450">
            {capitalize(pokemon?.name)}
          </h1>
          <p className="font-regular text-14 text-gray-400">
            {
              pokemon?.speciesDetail?.genera?.filter(
                (item) => item?.language?.name === "en"
              )[0]?.genus
            }
          </p>
        </div>

        <div className="flex flex-row space-x-8">
          {pokemon?.types?.map((item, index) => {
            return (
              <p
                key={index}
                className={`${
                  colorVariants[item?.type?.name]?.text
                } font-semibold text-14`}
              >
                {item?.type?.name?.toUpperCase()}
              </p>
            );
          })}
        </div>
        <div className="flex flex-row space-x-8">
          {pokemon?.abilities?.map((item, index) => (
            <p key={index} className="font-medium text-14 text-gray-400">
              {capitalize(item?.ability?.name)}
              {item?.is_hidden ? (
                <span className="italic text-gray-200">(hidden)</span>
              ) : null}
            </p>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PokeCard;

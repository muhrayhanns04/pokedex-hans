import React from "react";
import { formatStringToTitleCase } from "@/helpers/helpers.js";
import { ConfigProvider, Progress } from "antd";

const PokeStats = ({ pokemon }) => {
  return (
    <div className="flex flex-col items-center justify-center relative w-full">
      {pokemon?.stats?.map((item, index) => (
        <div
          className="grid grid-cols-[0.5fr,0.3fr,1fr] gap-8 w-full"
          key={index}
        >
          <p className="font-medium text-gray-300 text-14">
            {formatStringToTitleCase(item?.stat?.name)}
          </p>
          <p className="font-medium text-gray-300 text-14">
            {item?.base_stat}%
          </p>
          <ConfigProvider
            theme={{
              token: {
                colorTextQuaternary: "#e1e4e8",
                fontFamily: "Regular",
                fontSize: 14,
                colorTextPlaceholder: "#6B6F77",
              },
            }}
          >
            <Progress
              percent={item?.base_stat}
              size={[200, 10]}
              showInfo={false}
            />
          </ConfigProvider>
        </div>
      ))}
    </div>
  );
};

export default PokeStats;

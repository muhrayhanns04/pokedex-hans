import React, { useEffect, useRef, useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { colorVariants } from "@/api/colors.js";
import { capitalize } from "lodash";
import { ConfigProvider, Progress } from "antd";
import Button from "@/components/Button/index.jsx";
import {
  formatNumericValue,
  formatStringToTitleCase,
} from "@/helpers/helpers.js";
import { usePokemenStore } from "@/api/pokemon/usePokemonStore.js";
import PokeCard from "@/pages/HomePage/partials/Cards/partials/Card/partials/PokeCard/index.js";
import PokeStats from "@/pages/HomePage/partials/Cards/partials/Card/partials/PokeStats/index.js";
import PokeButtonDetail from "@/pages/HomePage/partials/Cards/partials/Card/partials/PokeButtonDetail/index.js";
import PokeContainer from "@/pages/HomePage/partials/Cards/partials/Card/partials/PokeContainer/index.js";
import { TbArrowRight } from "react-icons/tb";

const Card = ({ pokemon, index }) => {
  const formattedIndex = String(index + 1).padStart(4, "0");
  const [isOpen, setIsOpen] = useState(false);
  const [pokemonEvolution, setPokemonEvolution] = useState({});

  return (
    <>
      <PokeContainer>
        <PokeCard formattedIndex={formattedIndex} pokemon={pokemon} />
        <PokeStats pokemon={pokemon} />
        <PokeButtonDetail
          pokemon={pokemon}
          setIsOpen={setIsOpen}
          setPokemonEvolution={setPokemonEvolution}
        />
      </PokeContainer>

      {isOpen ? (
        <div
          className={`grid grid-cols-2 py-16 gap-16 px-16 ${
            index % 2 === 1 ? "bg-gray-30" : "bg-white"
          }`}
        >
          <div className="flex flex-row items-center space-x-16 items-center justify-center">
            {pokemonEvolution &&
              pokemonEvolution?.map((evolution, index) => (
                <>
                  <div
                    className="flex flex-col items-center justify-center"
                    key={index}
                  >
                    <LazyLoadImage
                      src={evolution?.sprites?.front_default}
                      className="w-[92px] z-20"
                    />
                    <div className="flex flex-col items-center justify-center space-y-12">
                      <div className="flex flex-col items-center justify-center">
                        <h1 className="font-bold text-24 text-gray-450">
                          {capitalize(evolution?.name)}
                        </h1>
                        <p className="font-regular text-14 text-gray-400">
                          {
                            evolution?.speciesDetail?.genera?.filter(
                              (item) => item?.language?.name === "en"
                            )[0]?.genus
                          }
                        </p>
                      </div>

                      <div className="flex flex-row space-x-8">
                        {evolution?.types?.map((item, index) => {
                          return (
                            <p
                              key={index}
                              className={`${
                                colorVariants[item?.type?.name]?.text
                              } font-semibold text-14`}
                            >
                              {item?.type?.name?.toUpperCase()}
                            </p>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  {index + 1 === pokemonEvolution?.length ? null : (
                    <TbArrowRight className="text-42 text-gray-200" />
                  )}
                </>
              ))}
          </div>
          <div className="relative flex flex-col w-full space-y-12">
            <h1 className="font-bold text-gray-500 text-16">Move List</h1>
            <div className="w-full flex flex-row gap-8 flex-wrap">
              {pokemon?.moves?.map((item, index) => (
                <div
                  className="bg-gray-20 rounded-full flex flex-col items-center justify-center px-8 py-4"
                  key={index}
                >
                  <p className="font-regular text-14 text-gray-500">
                    {formatStringToTitleCase(item?.move?.name)}
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Card;

import React, { useEffect, useRef } from "react";
import { usePokemenStore } from "@/api/pokemon/usePokemonStore.js";
import Card from "./partials/Card";
import { Spin } from "antd";

const Cards = () => {
  const [
    getPokemon,
    getPokemonSpecies,
    poke,
    addPokemon,
    isLoading,
    setIsLoading,
  ] = usePokemenStore((state) => [
    state.getPokemon,
    state.getPokemonSpecies,
    state.pokemons,
    state.addPokemon,
    state.isLoading,
    state.setIsLoading,
  ]);
  const isInit = useRef(false);

  const init = async () => {
    // get first data
    setIsLoading(true);

    await getPokemon({
      params: {
        limit: 25,
        offset: 0,
      },
      onSuccess: async (pokemonNonDetail) => {
        const results = pokemonNonDetail?.data?.results;

        const pokemonPromises = results?.map(async (item) => {
          const pokemon = await getPokemon({ id: item?.name });
          const species = await getPokemonSpecies({ id: pokemon?.data?.id });

          const data = {
            id: pokemon?.data?.id,
            name: pokemon?.data?.name,
            abilities: pokemon?.data?.abilities,
            height: pokemon?.data?.height,
            weight: pokemon?.data?.weight,
            moves: pokemon?.data?.moves,
            sprites: pokemon?.data?.sprites,
            stats: pokemon?.data?.stats,
            types: pokemon?.data?.types,
            speciesDetail: {
              base_happiness: species?.data?.base_happiness,
              genera: species?.data?.genera,
              habitat: species?.data?.habitat,
              shape: species?.data?.shape,
            },
          };

          return data;
        });

        const allPokemonData = await Promise.all(pokemonPromises);

        allPokemonData.forEach((data) => addPokemon(data));

        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      },
    });
  };

  useEffect(() => {
    if (!isInit.current) {
      init();
      isInit.current = true;
    }
  }, []);

  return (
    <div className="w-full h-full max-h-screen overflow-y-scroll border border-red-600">
      <div className="grid grid-cols-1 w-full">
        {isLoading ? (
          <div className="flex flex-col items-center justify-center w-full h-full">
            <Spin />
          </div>
        ) : (
          poke?.map((pokemon, index) => (
            <Card pokemon={pokemon} key={index} index={index} />
          ))
        )}
      </div>
    </div>
  );
};

export default Cards;

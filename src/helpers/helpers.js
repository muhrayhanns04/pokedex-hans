export const sanitizeFileName = (fileName) => {
  const forbiddenChars = /[<>:"/\\|?*\x00-\x1F]/g; // Regular expression to match forbidden characters
  const replacementChar = "_"; // Character to replace forbidden characters with

  // Remove forbidden characters and replace them with the replacement character
  const sanitizedFileName = fileName.replace(forbiddenChars, replacementChar);

  return sanitizedFileName;
};

export const isEmptyObject = (obj) => Object.keys(obj).length === 0;

export function formatStringToTitleCase(inputString) {
  // Split the string by hyphens or spaces
  const words = inputString.split(/-| /);

  // Capitalize the first letter of each word and join them with a space
  const formattedString = words
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");

  return formattedString;
}

export function formatNumericValue(value, options = {}) {
  const {
    prefix = "",
    suffix = "",
    decimals = 2,
    thousandsSeparator = ".",
    decimalSeparator = ",",
    negativeInRed = false,
  } = options;

  // Check if the value is numeric
  if (typeof value !== "number" || isNaN(value)) {
    return "NaN";
  }

  // Apply rounding and convert to a string with the specified number of decimals
  const formattedValue = value.toFixed(decimals);

  // Split into integer and decimal parts
  const [integerPart, decimalPart] = formattedValue.split(".");

  // Add thousands separator to the integer part
  const integerWithSeparator = integerPart.replace(
    /\B(?=(\d{3})+(?!\d))/g,
    thousandsSeparator
  );

  // Combine integer and decimal parts with the decimal separator
  const result =
    integerWithSeparator + (decimalPart ? decimalSeparator + decimalPart : "");

  // Add prefix and suffix
  const formattedResult = `${prefix}${result}${suffix}`;

  // Apply red color to negative values if required
  if (negativeInRed && value < 0) {
    return `<span style="color: red;">${formattedResult}</span>`;
  }

  return formattedResult;
}
export const hexToRgba = (hex, opacity) => {
  hex = hex?.replace(/^#/, ""); // Remove '#' if it's present
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);
  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

export const calculateToastDuration = (message) => {
  // Calculate the length of the message
  const messageLength = message.length;

  // Adjust the duration based on the message length
  // You can tweak the values according to your preference
  const duration = Math.max(messageLength * 40, 2000);

  return duration;
};

export const capitalize = (str) => {
  return str.replace(/_/g, " ").replace(/\b\w/g, function (match) {
    return match.toUpperCase();
  });
};

export const convertObjectToString = (obj, keys = []) => {
  const keyValuePairs = Object.entries(obj).map(([key, value]) => {
    if (typeof value === "object") {
      return convertObjectToString(value, keys.concat(key));
    } else {
      return `${[...keys, key].join(".")}=${value}`;
    }
  });
  return keyValuePairs.join("\n");
};

export const convertStringToObject = (string) => {
  return string?.split("\n").reduce((acc, cur) => {
    const [key, value] = cur?.split("=");
    const keys = key.split(".");

    let obj = acc;
    for (let i = 0; i < keys.length - 1; i++) {
      if (!obj[keys[i]]) obj[keys[i]] = {};
      obj = obj[keys[i]];
    }
    obj[keys[keys.length - 1]] = value;

    return acc;
  }, {});
};
export const parseFloatHelper = (e) => parseFloat(e)?.toFixed(2);
export const bytesToMegaBytes = (bytes) => bytes / 1024 ** 2;
export const valuesInObjectToNull = (obj) => {
  const newObj = Object?.keys(obj)?.reduce((accumulator, key) => {
    return { ...accumulator, [key]: null };
  }, {});

  return newObj;
};

export function numFormatter(num) {
  if (num > 999 && num < 1000000) {
    return num / 1000 + "K"; // convert to K for number from > 1000 < 1 million
  } else if (num > 1000000 && num < 1000000000) {
    return num / 1000000 + "M"; // convert to M for number from > 1 million
  } else if (num > 1000000000) {
    return num / 1000000000 + "B"; // convert to M for number from > 1 million
  } else if (num < 900) {
    return num; // if value < 1000, nothing to do
  }
}

export const getFilterDataLocal = (label, data, key) => {
  let newData = [{ label: `Semua ${label}`, value: "all" }];
  data?.reduce((a, b) => {
    const found = newData?.find((itemSearch) => itemSearch?.value == b[key]);

    if (!found) {
      return newData?.push({
        label: b[key],
        value: b[key],
      });
    }
  }, []);

  return newData;
};

export const monthNames = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

export const Number = (x) => {
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  } else {
    return x;
  }
};

export const GlobalDebug = (function () {
  var savedConsole = console;
  /**
   * @param {boolean} debugOn
   * @param {boolean} suppressAll
   */
  return function (debugOn, suppressAll) {
    var suppress = suppressAll || false;
    if (debugOn === false) {
      // supress the default console functionality
      console = {};
      console.log = function () {};
      // supress all type of consoles
      if (suppress) {
        console.info = function () {};
        console.warn = function () {};
        console.error = function () {};
      } else {
        console.info = savedConsole.info;
        console.warn = savedConsole.warn;
        console.error = savedConsole.error;
      }
    } else {
      console = savedConsole;
    }
  };
})();

export const omit = (obj, arr) =>
  Object?.keys(obj)
    .filter((k) => !arr.includes(k))
    .reduce((acc, key) => ((acc[key] = obj[key]), acc), {});

export function objectWithoutProperties(obj, keys) {
  var target = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object?.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
}

export const reduceCount = (data, key) => {
  return data?.reduce((total, item) => total + parseInt(item[key]), 0);
};

export const getRandomColor = () => {
  var letters = "BCDEF".split("");
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * letters.length)];
  }
  return color;
};

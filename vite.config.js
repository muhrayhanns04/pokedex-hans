import { defineConfig } from "vite";
import path from "path";
import react from "@vitejs/plugin-react";
import { splitVendorChunkPlugin } from "vite";
import svgr from "vite-plugin-svgr";

// const path = require("path");

// https://vitejs.dev/config/
export default defineConfig({
  base: "./",
  watch: {
    usePolling: true,
  },
  plugins: [react(), svgr(), splitVendorChunkPlugin()],
  server: {
    port: 3000,
    //   new
    fs: {
      allow: [".."],
    },
  },
  resolve: {
    extensions: [".mjs", ".js", ".ts", ".jsx", ".tsx", ".vue"],
    alias: [{ find: "@", replacement: path.resolve(__dirname, "./src") }],
    // new
    // alias: [{ find: "@", replacement: "/src" }],
  },
});
